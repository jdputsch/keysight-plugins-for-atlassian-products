package com.keysight.database.macros;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.storage.macro.MacroId;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.mail.template.ConfluenceMailQueueItem;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.plugin.services.VelocityHelperService;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.core.task.MultiQueueTaskManager;
import com.atlassian.mail.queue.MailQueueItem;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.keysight.database.helpers.DatabaseQueryHelper;
import com.keysight.database.helpers.ConnectionProfile;
import com.keysight.database.helpers.PluginConfigManager;
import com.keysight.database.helpers.InsertLogEntryIntoAuditDatabase;
import com.keysight.database.helpers.mail.MailService;
import com.keysight.database.helpers.mail.MailServiceImpl;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import com.atlassian.confluence.xhtml.api.MacroDefinition;
import com.atlassian.fugue.Option;
import com.atlassian.renderer.v2.RenderUtils;

import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheLoader;
import com.atlassian.cache.CacheManager;
import com.atlassian.cache.CacheSettingsBuilder;

import java.util.concurrent.TimeUnit;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.*;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.atlassian.confluence.mail.template.ConfluenceMailQueueItem.MIME_TYPE_HTML;
import static com.atlassian.confluence.mail.template.ConfluenceMailQueueItem.MIME_TYPE_TEXT;

public class DatabaseQuery implements Macro {
    private static final Logger log = LoggerFactory.getLogger(DatabaseQuery.class);

    private final static String PROFILE = "profile";
    private final static String QUERY_ISVISIBLE = "show-query";
    private final static String COLLAPSE_QUERY = "collapse-query";
    private final static String SQL = "sql";
    private final static String CACHE_EXPIRATION = "cache-expiration";
    private final static String BOUNDING_BOX = "bounding-box";
    private final static String REFRESH_BUTTON = "refresh-button";

    protected final CacheManager cacheManager;
    protected final PageManager pageManager;
    protected final PluginConfigManager pluginConfigManager;
    protected final PluginSettingsFactory pluginSettingsFactory;
    protected final SettingsManager settingsManager;
    protected final SpaceManager spaceManager;
    protected final MultiQueueTaskManager taskManager;
    protected final TransactionTemplate transactionTemplate;
    protected final VelocityHelperService velocityHelperService;
    protected final MailService mailService;

    private Connection connect = null;
    private Statement statement = null;
    private ResultSet resultSet = null;

    protected Map<String, String> parameters = null;
    protected String body = null;
    protected ConversionContext context = null;
    protected Cache<String, String> dataCache = null;
    protected Cache<String, String> creationTimeCache = null;

    public DatabaseQuery( CacheManager cacheManager,
                          PageManager pageManager,
                          PluginConfigManager pluginConfigManager,
                          PluginSettingsFactory pluginSettingsFactory,
                          SettingsManager settingsManager,
                          SpaceManager spaceManager,
                          MultiQueueTaskManager taskManager,
                          TransactionTemplate transactionTemplate,
                          VelocityHelperService velocityHelperService
    ) {
        this.cacheManager = cacheManager;
        this.pageManager = pageManager;
        this.pluginConfigManager = pluginConfigManager;
        this.pluginSettingsFactory = pluginSettingsFactory;
        this.settingsManager = settingsManager;
        this.spaceManager    = spaceManager;
        this.taskManager     = taskManager;
        this.transactionTemplate = transactionTemplate;
        this.velocityHelperService = velocityHelperService;

        this.mailService = new MailServiceImpl( taskManager );

        dataCache = cacheManager.getCache(DatabaseQuery.class.getName() + "data.cache",
                    new DatabaseQueryDataCacheLoader(),
                    new CacheSettingsBuilder().expireAfterWrite( pluginConfigManager.getMaxCacheLifeTimeInDaysAsLong(), TimeUnit.DAYS).build()
        );

        creationTimeCache = cacheManager.getCache(DatabaseQuery.class.getName() + ".create-time.cache",
                new DatabaseQueryCreateTimeCacheLoader(),
                new CacheSettingsBuilder().expireAfterWrite( pluginConfigManager.getMaxCacheLifeTimeInDaysAsLong(), TimeUnit.DAYS).build()
        );
    }

    public String execute(Map<String, String> parameters, String body, ConversionContext context)
            throws MacroExecutionException {

        this.parameters = parameters;
        this.body = body;
        this.context = context;

        String macroId = null;
        int expiration = 0;

        try {
            if (parameters.containsKey(CACHE_EXPIRATION)) {
                expiration = Integer.parseInt(parameters.get(CACHE_EXPIRATION));
            }
        } catch( Exception exception ){
        }

        //System.out.println( "\n\n\nCACHE Expirarion: "  + expiration + ")\n\n\n" );

        if( expiration > 0 ) {
            try {
                MacroDefinition macroDefinition = (MacroDefinition)context.getProperty("macroDefinition");
                Option<MacroId> option = macroDefinition.getMacroId();
                macroId = option.get().getId();

                // Clear the cache entry if it's older than the window set in the macro properties
                long creationTime = Long.parseLong(creationTimeCache.get(macroId));
                if( creationTime + ((long)expiration * (long)60 * (long)1000 ) < System.currentTimeMillis() )
                {
                    //System.out.println("Clear " + macroId + " From the cache\n" );
                    creationTimeCache.remove( macroId );
                    dataCache.remove( macroId );
                }
                //System.out.println("Try to get Data from CACHE (MacroID: " + macroId + ")\n");
                return dataCache.get(macroId);
            } catch (NoSuchElementException exception) {
                //System.out.println("Cache Retrival aborted: " + exception.getMessage() + "\n");
                return tryRenderDatabaseQuery(null);
            }
        } else {
            return tryRenderDatabaseQuery(null);
        }
    }

    private class DatabaseQueryCreateTimeCacheLoader implements CacheLoader<String, String>
    {
        @Override
        public String load( String creationTimeInMillisFromLong )
        {
            return Long.toString( System.currentTimeMillis() );
        }
    }

    private class DatabaseQueryDataCacheLoader implements CacheLoader<String, String>
    {
        @Override
        public String load( String macroId )
        {
            return tryRenderDatabaseQuery( macroId );
        }
    }

    private String tryRenderDatabaseQuery( String macroId )
    {
        try
        {
            return renderDatabaseQuery(macroId);
        }
        catch( Exception exception )
        {
            return RenderUtils.blockError("Macro Execution Error:", exception.getMessage());
        }
    }

    public String renderDatabaseQuery( String macroId ) throws MacroExecutionException {
        // if the macroId is known, reset the timestamp for the creationTimeCache entry.
        if( macroId != null )
        {
            try {
                creationTimeCache.remove(macroId);
            }
            catch( Exception exception ) {
            }
            creationTimeCache.get(macroId);
        }

        //System.out.println( "\n\n\nGet Data from real Source (MacroID: "  + macroId + ")\n\n\n" );
        String baseUrl = settingsManager.getGlobalSettings().getBaseUrl();
        String template = "/com/keysight/database/templates/database-query.vm";
        Map<String, Object> velocityContext = velocityHelperService.createDefaultVelocityContext();

        String fullName = null;
        String userName = null;
        ConfluenceUser currentUser = AuthenticatedUserThreadLocal.get();

        if (currentUser != null) {
            fullName = currentUser.getFullName();
            userName = currentUser.getName();
        } else {
            fullName = "Anonymous";
            userName = "anonymous";
        }

        String spaceKey = this.context.getSpaceKey();
        String spaceName = spaceManager.getSpace( spaceKey ).getName();
        String pageName = this.context.getEntity().getTitle();
        String pageUrl = baseUrl + this.context.getEntity().getUrlPath();
        String activeProfileId = null;
        ConnectionProfile profile = null;

        String sql;
        if( this.parameters.containsKey( SQL ) ) {
            sql = this.parameters.get( SQL );
        } else {
            sql = this.body;
        }

        // Clean up body by removing extra whitespace, newlines, etc.
        String rawSql = sql;
        sql = sql.replaceAll("\\s+", " ");

        ArrayList<String> columnNames = new ArrayList<>();
        ArrayList<ArrayList<String>> dataRows = new ArrayList<>();

        if( this.parameters.containsKey( PROFILE ))
        {
            activeProfileId = this.parameters.get(PROFILE);

            // needed for Confluence Data Center as the config could have been
            // updated by an instance of Confluence other than this one.
            pluginConfigManager.loadFromStorage();

            profile = pluginConfigManager.getConnectionProfileFromId( activeProfileId );
            if (profile == null) {
                throw new MacroExecutionException("No profile found with that ID for which you are authorized: " + activeProfileId);
            }
            connect = DatabaseQueryHelper.createConnection(profile, pluginConfigManager);
        } else {
            throw new MacroExecutionException("No profile selected");
        }

        if (connect != null) {
            try {
                // Statements allow to issue SQL queries to the database
                statement = connect.createStatement();

                // Check for query constraints [pre]
                // We will set the soft timer arbitrarily high
                int softTimer = Integer.MAX_VALUE;
                int startQuery = (int) ((new Date()).getTime() / 1000); // Current time in seconds
                if (!Objects.equals(pluginConfigManager.getTimeoutLimitValue(), "")) {
                    // Try to get integer value of timeout value, default to 0
                    int timeVal;
                    try {
                        timeVal = Integer.parseInt(pluginConfigManager.getTimeoutLimitValue());
                    } catch (NumberFormatException e) {
                        // 0 seconds is interpreted as no time limit.
                        throw new MacroExecutionException("Failed to parse query timeout: " + pluginConfigManager.getTimeoutLimitValue());
                    }

                    // Soft or hard?
                    if (Objects.equals(StringUtils.lowerCase(pluginConfigManager.getTimeoutLimitType()), "soft")) {
                        softTimer = timeVal;
                    } else {
                        statement.setQueryTimeout(timeVal);
                    }
                }

                // Handle row limit
                int softRowLimit = Integer.MAX_VALUE;
                if (!Objects.equals(pluginConfigManager.getRowLimitValue(), "")) {
                    // Try to get integer value of timeout value, default to 0
                    int rowLimit;
                    try {
                        rowLimit = Integer.parseInt(pluginConfigManager.getRowLimitValue());
                    } catch (NumberFormatException e) {
                        // 0 seconds is interpreted as no time limit.
                        throw new MacroExecutionException("Failed to parse query row limit: " + pluginConfigManager.getTimeoutLimitValue());
                    }

                    // Soft or hard?
                    if (Objects.equals(StringUtils.lowerCase(pluginConfigManager.getRowLimitType()), "soft")) {
                        softRowLimit = rowLimit;
                    } else {
                        statement.setMaxRows(rowLimit);
                    }
                }

                // Result set get the result of the SQL query
                resultSet = statement.executeQuery(sql);

                // Check for query constraints [post]
                int endQuery = (int) ((new Date()).getTime() / 1000); // Current time in seconds

                // Did more than softTimer pass?
                if (endQuery - startQuery > softTimer) {
                    String message = "<p>The user <b>" + fullName + " ( " + userName + " )</b> executed an sql query\n "
                            + "using the Database Connection Macro on the page <a href=\"" + pageUrl + "\">" + pageName + "</a>\n "
                            + "in the space <b>" + spaceName + "</b>. It took " + (endQuery - startQuery) + " seconds\n"
                            + "exceeding the soft time limit of " + softTimer + ".</p>\n";

                    sendEmail(pluginConfigManager, "Confluence DB Connctory Soft Timout Limit Warning", message, null);
                }

                // Get the Column Names
                for (int i = 1; i <= resultSet.getMetaData().getColumnCount(); i++) {
                    columnNames.add(resultSet.getMetaData().getColumnLabel(i));
                }
                velocityContext.put("columnNames", columnNames);

                // Get the Column Names
                int rowCount = 0;
                while (resultSet.next()) {
                    rowCount++;
                    ArrayList<String> row = new ArrayList<String>();
                    for (int i = 1; i <= resultSet.getMetaData().getColumnCount(); i++) {
                        row.add(resultSet.getString(i));
                    }
                    dataRows.add(row);
                }

                Date entryDate = new Date(System.currentTimeMillis());

                // Did we pass the soft row limit?
                if (rowCount > softRowLimit) {

                    String message = "<p>The user <b>" + fullName + " ( " + userName + " )</b> executed an sql query\n "
                                    + "using the Database Connection Macro on the page <a href=\"" + pageUrl + "\">" + pageName + "</a>\n "
                                    + "in the space <b>" + spaceName + "</b>. It returned " + rowCount + " rows\n "
                                    + "exceeding the soft row limit of " + softRowLimit + ".</p>\n";

                    sendEmail(pluginConfigManager, "Confluence DB Connector Soft Row Limit Warning", message, null);
                }

                String logMessage = "DB Query( "
                                  + "fullName="+fullName+", "
                                  + "userName="+userName+", "
                                  + "pageName="+pageName+", "
                                  + "spaceName="+spaceName+", "
                                  + "databaseProfile="+profile.getName()+", "
                                  + "rowCount="+rowCount+", "
                                  + "queryDuration="+(endQuery - startQuery)+")\n";

                if( pluginConfigManager.getAtlassianLogLevel().equals( "Warn" ))
                {
                    log.warn( logMessage );
                }
                else
                {
                    log.info( logMessage );
                }

                ConnectionProfile auditLogProfile = pluginConfigManager.getConnectionProfileFromId( pluginConfigManager.getAuditLogDbProfile() );
                if( auditLogProfile != null )
                {
                   Runnable runnable = new InsertLogEntryIntoAuditDatabase( auditLogProfile,
                                                                            pluginConfigManager,
                                                                            profile,
                                                                            fullName,
                                                                            userName,
                                                                            pageUrl,
                                                                            pageName,
                                                                            spaceName,
                                                                            rowCount,
                                                                            (endQuery - startQuery),
                                                                            rawSql );
                    new Thread(runnable).start();
                }

                if (pluginConfigManager.getLogEmail() != null && !Objects.equals(pluginConfigManager.getLogEmail(), "")) {
                    String delimiter = "-----field-delimiter\n";
                    String keyValueSplitter = "-----key-value-splitter\n";
                    String message = null;

                    if( pluginConfigManager.getEmailContentFormat().equals( "XML" )) {
                        message = "<database-connector-log-entry>\n"
                                + "   <fullName><![CDATA[" + fullName + "]]></fullName>\n"
                                + "   <userName><![CDATA[" + userName + "]]></userName>\n"
                                + "   <pageName><![CDATA[" + pageName + "]]></pageName>\n"
                                + "   <pageUrl><![CDATA[" + pageUrl + "]]></pageUrl>\n"
                                + "   <spaceName><![CDATA[" + spaceName + "]]></spaceName>\n"
                                + "   <databaseProfile><![CDATA[" + profile.getName() + "]]></databaseProfile>\n"
                                + "   <rowCount><![CDATA[" + rowCount + "]]></rowCount>\n"
                                + "   <queryDuration><![CDATA[" + (endQuery - startQuery) + "]]></queryDuration>\n"
                                + "   <sqlQuery><![CDATA[" + rawSql + "]]></sqlQuery>\n"
                                + "   <timestamp><![CDATA[" + LocalDateTime.now() + "]]></timestamp>\n"
                                + "</database-connector-log-entry>\n";
                    } else if( pluginConfigManager.getEmailContentFormat().equals( "JSON" )) {
                        message = "{\"fullName\":\"" +StringEscapeUtils.escapeJson(fullName)+"\","
                                + "\"userName\":\"" +StringEscapeUtils.escapeJson(userName)+"\","
                                + "\"pageName\":\"" +StringEscapeUtils.escapeJson(pageName)+"\","
                                + "\"pageUrl\":\""  +StringEscapeUtils.escapeJson(pageUrl)+"\","
                                + "\"databaseProfile\":\""  +StringEscapeUtils.escapeJson(profile.getName())+"\","
                                + "\"spaceName\":\""+StringEscapeUtils.escapeJson(spaceName)+"\","
                                + "\"rowCount\":\"" +rowCount+"\","
                                + "\"queryDuration\":\""+(endQuery - startQuery)+"\","
                                + "\"sqlQuery\":\"" +StringEscapeUtils.escapeJson(StringEscapeUtils.escapeJson(rawSql))+"\","
                                + "\"timestamp\":\""     +LocalDateTime.now()+"\"}";
                    } else if( pluginConfigManager.getEmailContentFormat().equals( "Parsable Text" )) {
                        message = delimiter + "fullName\n" + keyValueSplitter + fullName + "\n"
                                + delimiter + "userName\n" + keyValueSplitter + userName + "\n"
                                + delimiter + "pageName\n" + keyValueSplitter + pageName + "\n"
                                + delimiter + "pageUrl\n" + keyValueSplitter + pageUrl + "\n"
                                + delimiter + "databaseProfile\n" + keyValueSplitter + profile.getName() + "\n"
                                + delimiter + "spaceName\n" + keyValueSplitter + spaceName + "\n"
                                + delimiter + "rowCount\n" + keyValueSplitter + rowCount + "\n"
                                + delimiter + "queryDuration\n" + keyValueSplitter + (endQuery - startQuery) + "\n"
                                + delimiter + "sqlQuery\n" + keyValueSplitter + rawSql + "\n"
                                + delimiter + "timestamp\n" + keyValueSplitter + LocalDateTime.now() + "\n";
                    } else {
                        message = "fullName = " + fullName + "\n"
                                + "userName = " + userName + "\n"
                                + "pageUrl = " + pageUrl + "\n"
                                + "pageName = " + pageName + "\n"
                                + "spaceName = " + spaceName + "\n"
                                + "databaseProfile = " + profile.getName() + "\n"
                                + "rowCount = " + rowCount + "\n"
                                + "queryDuration = " + (endQuery - startQuery) + "\n"
                                + "timestamp = " + LocalDateTime.now() + "\n"
                                + "sqlQuery\n"
                                + "--------\n"
                                + rawSql + "\n";
                    }

                    sendPlainTextEmail(pluginConfigManager, "DB Connector Log Entry: Returned " + rowCount + " rows in " + (endQuery - startQuery) + " seconds at " + LocalDateTime.now(), message, pluginConfigManager.getLogEmail());
                }

                velocityContext.put("dataRows", dataRows);
            } catch (Exception e) {
                throw new MacroExecutionException(e);
            } finally {
                close();
            }
        }

        StringBuilder codeViewOfBody = new StringBuilder();

        codeViewOfBody.append("<ac:structured-macro ac:name=\"code\" ac:schema-version=\"1\">");
        codeViewOfBody.append("<ac:parameter ac:name=\"language\">sql</ac:parameter>");
        codeViewOfBody.append("<ac:parameter ac:name=\"title\">SQL Query</ac:parameter>");
        if (this.parameters.containsKey(COLLAPSE_QUERY)) {
            codeViewOfBody.append("<ac:parameter ac:name=\"collapse\">true</ac:parameter>");
        }
        codeViewOfBody.append("   <ac:plain-text-body><![CDATA[");
        codeViewOfBody.append(rawSql);
        codeViewOfBody.append("]]></ac:plain-text-body>");
        codeViewOfBody.append("</ac:structured-macro>");

        String databaseQueryContainerCssClass = "";
        String boxCssClass = "";
        if( parameters.containsKey( BOUNDING_BOX )){
            databaseQueryContainerCssClass = "panel database-query-container";
            boxCssClass = "database-query-box database-query-top database-query-body-container database-query-bottom";
            velocityContext.put("boxCssClass", boxCssClass);
        } else {
            databaseQueryContainerCssClass = "database-query-container";
        }
        velocityContext.put("databaseQueryContainerCssClass", databaseQueryContainerCssClass);

        if( parameters.containsKey(REFRESH_BUTTON) && macroId != null ){
            velocityContext.put("showRefreshMacroId", "true");
            velocityContext.put("refreshMacroId", macroId);
        }

        velocityContext.put("showQuery", this.parameters.get(QUERY_ISVISIBLE));
        velocityContext.put("code", codeViewOfBody.toString());
        return velocityHelperService.getRenderedTemplate(template, velocityContext);
    }

    private void sendEmail(PluginConfigManager pluginConfigManager, String subject, String body, String customEmail) {
        sendEmailLowLevel( pluginConfigManager, subject, body, customEmail, MIME_TYPE_HTML);
    }

    private void sendPlainTextEmail(PluginConfigManager pluginConfigManager, String subject, String body, String customEmail) {
        sendEmailLowLevel( pluginConfigManager, subject, body, customEmail, MIME_TYPE_TEXT);
    }

    private void sendEmailLowLevel(PluginConfigManager pluginConfigManager, String subject, String body, String customEmail, String mimeType ) {
        // needed for Confluence Data Center as the config could have been
        // updated by an instance of Confluence other than this one.
        pluginConfigManager.loadFromStorage();

        if (customEmail == null || Objects.equals(customEmail, "")) {
            customEmail = pluginConfigManager.getNotificationEmail();
        }

        try{
            MailQueueItem mailQueueItem = new ConfluenceMailQueueItem(customEmail, subject, body, mimeType);
            mailService.sendEmail( mailQueueItem );
        } catch( Exception e ){
            System.out.println( "Failed to send email." );
            System.out.println( e.getMessage() );
        }
    }

    private void close() {
        try {
            if (resultSet != null) {
                resultSet.close();
            }
            if (statement != null) {
                statement.close();
            }
            if (connect != null) {
                connect.close();
            }
        } catch (Exception e) {

        }
    }

    public BodyType getBodyType() {
        return BodyType.PLAIN_TEXT;
    }

    public OutputType getOutputType() {
        return OutputType.BLOCK;
    }
}
