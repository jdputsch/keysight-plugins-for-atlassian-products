package com.keysight.database.macros;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.storage.macro.MacroId;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.plugin.services.VelocityHelperService;
import com.keysight.database.helpers.PluginConfigManager;
import org.apache.commons.lang3.StringUtils;
import com.atlassian.confluence.xhtml.api.MacroDefinition;
import com.atlassian.fugue.Option;
import com.atlassian.renderer.v2.RenderUtils;

import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheLoader;
import com.atlassian.cache.CacheManager;
import com.atlassian.cache.CacheSettingsBuilder;
import com.atlassian.confluence.cluster.ClusterManager;
import com.atlassian.confluence.cluster.ClusterNodeInformation;

import java.util.concurrent.TimeUnit;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.*;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MacroResultsCache implements Macro {
    private static final Logger log = LoggerFactory.getLogger(MacroResultsCache.class);

    private final static String CACHE_EXPIRATION = "cache-expiration";
    private final static String BOUNDING_BOX = "bounding-box";
    private final static String REFRESH_BUTTON = "refresh-button";

    protected final CacheManager cacheManager;
    protected final ClusterManager clusterManager;
    protected final PluginConfigManager pluginConfigManager;
    protected final VelocityHelperService velocityHelperService;

    protected Map<String, String> parameters = null;
    protected String body = null;
    protected ConversionContext context = null;
    protected Cache<String, String> dataCache = null;
    protected Cache<String, String> creationTimeCache = null;

    public MacroResultsCache( CacheManager cacheManager,
                              ClusterManager clusterManager,
                              PluginConfigManager pluginConfigManager,
                              VelocityHelperService velocityHelperService
    ) {
        this.cacheManager = cacheManager;
        this.clusterManager = clusterManager;
        this.pluginConfigManager = pluginConfigManager;
        this.velocityHelperService = velocityHelperService;

        // needed for Confluence Data Center as the config could have been
        // updated by an instance of Confluence other than this one.
        pluginConfigManager.loadFromStorage();

        dataCache = cacheManager.getCache(MacroResultsCache.class.getName() + "data.cache",
                    new MacroResultsCacheDataCacheLoader(),
                    new CacheSettingsBuilder().expireAfterWrite( pluginConfigManager.getMaxCacheLifeTimeInDaysAsLong(), TimeUnit.DAYS).build()
        );

        creationTimeCache = cacheManager.getCache(MacroResultsCache.class.getName() + ".create-time.cache",
                new MacroResultsCacheCreateTimeCacheLoader(),
                new CacheSettingsBuilder().expireAfterWrite( pluginConfigManager.getMaxCacheLifeTimeInDaysAsLong(), TimeUnit.DAYS).build()
        );
    }

    public String execute(Map<String, String> parameters, String body, ConversionContext context)
            throws MacroExecutionException {
        this.parameters = parameters;
        this.body = body;
        this.context = context;

        String macroId = null;
        int expiration = 0;
        String clusterNodeId = null;

        try {
            if (parameters.containsKey(CACHE_EXPIRATION)) {
                expiration = Integer.parseInt(parameters.get(CACHE_EXPIRATION));
            }
        } catch( Exception exception ){
        }

        if( expiration > 0 ) {
            try {
                if( clusterManager.isClustered() ){
                    clusterNodeId = clusterManager.getThisNodeInformation().getAnonymizedNodeIdentifier() + ":";
                } else {
                    clusterNodeId = "";
                }

                MacroDefinition macroDefinition = (MacroDefinition)context.getProperty("macroDefinition");
                Option<MacroId> option = macroDefinition.getMacroId();
                macroId = clusterNodeId + option.get().getId();

                // Clear the cache entry if it's older than the window set in the macro properties
                long creationTime = Long.parseLong(creationTimeCache.get(macroId));
                if( creationTime + ((long)expiration * (long)60 * (long)1000 ) < System.currentTimeMillis() )
                {
                    creationTimeCache.remove( macroId );
                    dataCache.remove( macroId );
                }
                return dataCache.get(macroId);

            } catch (NoSuchElementException exception) {
                return tryRenderMacro(null);
            }
        } else {
            return tryRenderMacro(null);
        }
    }

    private class MacroResultsCacheCreateTimeCacheLoader implements CacheLoader<String, String>
    {
        @Override
        public String load( String creationTimeInMillisFromLong )
        {
            return Long.toString( System.currentTimeMillis() );
        }
    }

    private class MacroResultsCacheDataCacheLoader implements CacheLoader<String, String>
    {
        @Override
        public String load( String macroId )
        {
            return tryRenderMacro( macroId );
        }
    }

    public String tryRenderMacro( String macroId ){
        try
        {
            return renderMacro(macroId);
        }
        catch( Exception exception )
        {
            return RenderUtils.blockError("Macro Execution Error:", exception.getMessage());
        }
    }

    public String renderMacro( String macroId ) throws MacroExecutionException {
        if( macroId != null )
        {
            try {
                creationTimeCache.remove(macroId);
            }
            catch( Exception exception ) {
            }
            creationTimeCache.get(macroId);
        }

        String boxCssClass = "";
        String panelCssClass = "";
        String refreshButton = "";

        String macroIdWithoutClusterNodeId = macroId;
        System.out.println( "Macro ID Without Cluster Node Id: " + macroIdWithoutClusterNodeId );
        String[] parts = macroId.split(":");
        if( parts.length > 1 ){
            macroIdWithoutClusterNodeId = parts[1];
            System.out.println( "Macro ID Without Cluster Node Id: " + macroIdWithoutClusterNodeId );
        }
        System.out.println( "Macro ID Without Cluster Node Id: " + macroIdWithoutClusterNodeId );

        if( parameters.containsKey( BOUNDING_BOX ) ) {
            panelCssClass = " panel";
            boxCssClass = "class=\"macro-results-cache-box macro-results-cache-top macro-results-cache-body-container macro-results-cache-bottom\"";
        }

        if( parameters.containsKey( REFRESH_BUTTON ) && macroIdWithoutClusterNodeId != null ) {
            refreshButton = "      <div class=\"macro-results-cache-refresh-button\">\n"
                          + "         <span class=\"refresh-button\">\n"
                          + "            <a class=\"icon icon-refresh\" rel=\"nofollow\" onclick=\"forceMacroResultsCacheCacheRefresh(\'"+macroIdWithoutClusterNodeId+"\');\"/>&nbsp;"
                          + "            <a onclick=\"forceMacroResultsCacheCacheRefresh(\'"+macroIdWithoutClusterNodeId+"\');\">Refresh</a>"
                          + "         </span>\n"
                          + "      </div>\n";
        } else {
            refreshButton = "";
        }

        return "<div class=\"macro-results-cache-container" + panelCssClass + "\">\n"
             + "   <div "+boxCssClass+">"
             + "      <div class=\"macro-result-cache-body\">"
             +           this.body+"\n"
             + "      </div>\n"
             +        refreshButton
             + "   </div>\n"
             + "</div>\n";
    }

    public BodyType getBodyType() {
        return BodyType.RICH_TEXT;
    }

    public OutputType getOutputType() {
        return OutputType.BLOCK;
    }
}
