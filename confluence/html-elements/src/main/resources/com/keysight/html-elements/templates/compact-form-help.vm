<h3>Introduction</h3>

<p>The Compact Form macro allows a user to create a form
on a confluence page using a compact plain text syntax in the body
of the macro.</p>

<h3>Form Syntax</h3>
<p>Every line in the body of the macro maps to a distinct form element.</p>

<h4>Text Field</h4>
<pre>
KEY
KEY->LABEL
</pre>
<p>A text field can be created by specifying a single string, such 
as <strong>KEY</strong>.  When the submit button is pressed, <strong>KEY</strong>
will be passed into the with whatever text is in the field as the <strong>VAlUE</strong>.
To provide a user friendly label on the web page, use the syntax <strong>KEY->LABEL</strong>.
If no label is specified, none will be printed.</p>
<p>Example: USERNAME->Username</p>

<h4>Hidden Field</h4>
<pre>
*KEY:VALUE
</pre>
<p>The hidden field will not be shown to the user on the web page, but
the <strong>KEY:VALUE</strong> pair will be passed to the form url when submitted.</p>
<p>Example: *SECRET_VALUE->BLUE</p>

<h4>Dropdown Selection</h4>
<pre>
KEY:SELECTION1,SELECTION2,!DEFAULT_SELECTION
KEY->LABEL:SELECTION1->LABEL1,SELECTION2->LABEL2,!DEFAULT_SELECTION->LABEL3
</pre>

<p>The form element is named by the <strong>KEY</strong>.  As with the Text Field, a user
friendly label can be provided with the syntax <strong>KEY->LABEL</strong>.  If the key is 
followed by a colon, and a comma separated list that indicates a dropdown
list should be created.  Each of the selections can be provided with a 
user friendly label as shown in the syntax.  If one of the selections is preceded
with an exclamation point (!), it will be the default selection.  Only one
selection should be flagged with the exclamation point.</p>
<p>Example: FAVORITE_COLOR->My Favorite Color:!BLUE->Awesome Blue,PINK->Gorgeous Pink</p>

<h3>Parameters</h3>
<p><strong>Style</strong>: Sets the display style for the form.  AUI looks better and fits the general Confluence look and feel.  Plain HTML is more compact.</p>
<p><strong>URL</strong>: Sets the url where the form data will be sent when submitted.</p>
<p><strong>Form Target</strong>: Determines if the results from the submitted form should be opened in the same window or a new one.</p>
<p><strong>Submit Button Text</strong>: Sets the text placed on the submit button.</p>
<h3>Parameters Exclusive to the AUI Style</h3>
<p><strong>Form Layout</strong>: Determines if the labels should be short on the left (default), long on the left, on the top or un-sectioned (for short forms).</p>
<h3>Parameters Exclusive to the Plain HTML Style</h3>
<p><strong>Form Element Separator</strong>: A string, such as a line break (&lt;br /&gt;) to insert between form elements.</p>
<p><strong>Form Alignment</strong>: The justification of the form elements: Left, Center or Right</p>
<p><strong>Submit Button Element Separator</strong>: A string, such as a line break (&lt;br /&gt;) to insert before the submit button. (Defaults to the Form Element Separator)</p>
<p><strong>Submit Button Alignment</strong>: The justification of the submit button: Left, Center or Right (Defaults to the Form Alignment)</p>

