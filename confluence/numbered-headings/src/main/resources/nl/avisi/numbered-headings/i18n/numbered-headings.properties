# General

# Error Messages
nl.avisi.numbered-headings.unable-to-render=Unable to render ''Numbered Headings'' macro
nl.avisi.numbered-headings.unexpected-error=An unexpected error has occurred, please create a bug at https://bitbucket.org/avisi/numbered-headings

# Numbered Headings Macro
nl.avisi.numbered-headings.numbered-headings.label=Numbered Headings
nl.avisi.numbered-headings.numbered-headings.desc=The Numbered Headings macro creates a block inside of which headings are automatically numbered in the specified format.<br /><a href="#" onclick="numberedHeadingsHelp.showNumberedHeadingsHelp( event );">Documentation</a>
nl.avisi.numbered-headings.numbered-headings.param.number-format.label=Number format
nl.avisi.numbered-headings.numbered-headings.param.number-format.desc=The format used for rendering the numbering
nl.avisi.numbered-headings.numbered-headings.param.start-numbering-with.label=Start numbering with
nl.avisi.numbered-headings.numbered-headings.param.start-numbering-with.desc=The value to start numbering with
nl.avisi.numbered-headings.numbered-headings.param.start-numbering-with.invalid-number='Start numbering with' value is not a valid number
nl.avisi.numbered-headings.numbered-headings.param.start-numbering-with.not-supported=Selected number format does not support numbering starting at {0}
nl.avisi.numbered-headings.numbered-headings.param.start-numbering-at.label=Start numbering at
nl.avisi.numbered-headings.numbered-headings.param.start-numbering-at.desc=The heading to start numbering at
nl.avisi.numbered-headings.numbered-headings.param.skip-headings.label=Skip headings
nl.avisi.numbered-headings.numbered-headings.param.skip-headings.desc=Comma separated list of headings which should be skipped. Example: h5, h6
nl.avisi.numbered-headings.numbered-headings.param.h1.label=h1 format
nl.avisi.numbered-headings.numbered-headings.param.h1.desc=Example: Chapter [h1.decimal].
nl.avisi.numbered-headings.numbered-headings.param.h2.label=h2 format
nl.avisi.numbered-headings.numbered-headings.param.h2.desc=Example: Paragraph [h1.decimal].[h2.upper-latin].
nl.avisi.numbered-headings.numbered-headings.param.h3.label=h3 format
nl.avisi.numbered-headings.numbered-headings.param.h3.desc=Example: Sub-Paragraph [h3.lower-roman].
nl.avisi.numbered-headings.numbered-headings.param.h4.label=h4 format
nl.avisi.numbered-headings.numbered-headings.param.h4.desc=If left empty, nothing will be rendered.
nl.avisi.numbered-headings.numbered-headings.param.h5.label=h5 format
nl.avisi.numbered-headings.numbered-headings.param.h5.desc=You can use any of the formatters from the dropdown above.
nl.avisi.numbered-headings.numbered-headings.param.h6.label=h6 format
nl.avisi.numbered-headings.numbered-headings.param.h6.desc=See the <a href="#" onclick="numberedHeadingsHelp.showNumberedHeadingsHelp( event );">documentation</a> for more examples.

# Numbered Headings For Page
nl.avisi.numbered-headings.numbered-headings-for-page.label=Numbered Headings For Page
nl.avisi.numbered-headings.numbered-headings-for-page.desc=When placed on a page, all headings on that page will be automatically numbered in the specified format.<br /><a href="#" onclick="numberedHeadingsHelp.showNumberedHeadingsHelp( event );">Documentation</a>
nl.avisi.numbered-headings.numbered-headings-for-page.param.number-format.label=Number format
nl.avisi.numbered-headings.numbered-headings-for-page.param.number-format.desc=The format used for rendering the numbering
nl.avisi.numbered-headings.numbered-headings-for-page.param.start-numbering-with.label=Start numbering with
nl.avisi.numbered-headings.numbered-headings-for-page.param.start-numbering-with.desc=The value to start numbering with
nl.avisi.numbered-headings.numbered-headings-for-page.param.start-numbering-with.invalid-number='Start numbering with' value is not a valid number
nl.avisi.numbered-headings.numbered-headings-for-page.param.start-numbering-with.not-supported=Selected number format does not support numbering starting at {0}
nl.avisi.numbered-headings.numbered-headings-for-page.param.start-numbering-at.label=Start numbering at
nl.avisi.numbered-headings.numbered-headings-for-page.param.start-numbering-at.desc=The heading to start numbering at
nl.avisi.numbered-headings.numbered-headings-for-page.param.skip-headings.label=Skip headings
nl.avisi.numbered-headings.numbered-headings-for-page.param.skip-headings.desc=Comma separated list of headings which should be skipped. Example: h5, h6
nl.avisi.numbered-headings.numbered-headings-for-page.param.h1.label=h1 format
nl.avisi.numbered-headings.numbered-headings-for-page.param.h1.desc=Example: Chapter [h1.decimal].
nl.avisi.numbered-headings.numbered-headings-for-page.param.h2.label=h2 format
nl.avisi.numbered-headings.numbered-headings-for-page.param.h2.desc=Example: Paragraph [h1.decimal].[h2.upper-latin].
nl.avisi.numbered-headings.numbered-headings-for-page.param.h3.label=h3 format
nl.avisi.numbered-headings.numbered-headings-for-page.param.h3.desc=Example: Sub-Paragraph [h3.lower-roman].
nl.avisi.numbered-headings.numbered-headings-for-page.param.h4.label=h4 format
nl.avisi.numbered-headings.numbered-headings-for-page.param.h4.desc=If left empty, nothing will be rendered.
nl.avisi.numbered-headings.numbered-headings-for-page.param.h5.label=h5 format
nl.avisi.numbered-headings.numbered-headings-for-page.param.h5.desc=You can use any of the formatters from the dropdown above.
nl.avisi.numbered-headings.numbered-headings-for-page.param.h6.label=h6 format

# Numbered Headings (Transition)
nl.avisi.numbered-headings.numberedheadings-transition.label=Numbered Headings (Transition)
nl.avisi.numbered-headings.numberedheadings-transition.desc=The Numbered Headings macro creates a block inside of which headings are automatically numbered in the specified format.<br /><a href="#" onclick="numberedHeadingsHelp.showNumberedHeadingsHelp( event );">Documentation</a>
nl.avisi.numbered-headings.numberedheadings-transition.param.number-format.label=Number format
nl.avisi.numbered-headings.numberedheadings-transition.param.number-format.desc=The format used for rendering the numbering
nl.avisi.numbered-headings.numberedheadings-transition.param.start-numbering-with.label=Start numbering with
nl.avisi.numbered-headings.numberedheadings-transition.param.start-numbering-with.desc=The value to start numbering with
nl.avisi.numbered-headings.numberedheadings-transition.param.start-numbering-with.invalid-number='Start numbering with' value is not a valid number
nl.avisi.numbered-headings.numberedheadings-transition.param.start-numbering-with.not-supported=Selected number format does not support numbering starting at {0}
nl.avisi.numbered-headings.numberedheadings-transition.param.start-numbering-at.label=Start numbering at
nl.avisi.numbered-headings.numberedheadings-transition.param.start-numbering-at.desc=The heading to start numbering at
nl.avisi.numbered-headings.numberedheadings-transition.param.skip-headings.label=Skip headings
nl.avisi.numbered-headings.numberedheadings-transition.param.skip-headings.desc=Comma separated list of headings which should be skipped. Example: h5, h6
nl.avisi.numbered-headings.numberedheadings-transition.param.h1.label=h1 format
nl.avisi.numbered-headings.numberedheadings-transition.param.h1.desc=Example: Chapter [h1.decimal].
nl.avisi.numbered-headings.numberedheadings-transition.param.h2.label=h2 format
nl.avisi.numbered-headings.numberedheadings-transition.param.h2.desc=Example: Paragraph [h1.decimal].[h2.upper-latin].
nl.avisi.numbered-headings.numberedheadings-transition.param.h3.label=h3 format
nl.avisi.numbered-headings.numberedheadings-transition.param.h3.desc=Example: Sub-Paragraph [h3.lower-roman].
nl.avisi.numbered-headings.numberedheadings-transition.param.h4.label=h4 format
nl.avisi.numbered-headings.numberedheadings-transition.param.h4.desc=If left empty, nothing will be rendered.
nl.avisi.numbered-headings.numberedheadings-transition.param.h5.label=h5 format
nl.avisi.numbered-headings.numberedheadings-transition.param.h5.desc=You can use any of the formatters from the dropdown above.
nl.avisi.numbered-headings.numberedheadings-transition.param.h6.label=h6 format

# Numbered Headings (Legacy)
nl.avisi.numbered-headings.numberedheadings.label=Numbered Headings (Legacy)
nl.avisi.numbered-headings.numberedheadings.desc=The Numbered Headings macro creates a block inside of which headings are automatically numbered in the specified format.<br /><a href="#" onclick="numberedHeadingsHelp.showNumberedHeadingsHelp( event );">Documentation</a>
nl.avisi.numbered-headings.numberedheadings.param.number-format.label=Number format
nl.avisi.numbered-headings.numberedheadings.param.number-format.desc=The format used for rendering the numbering
nl.avisi.numbered-headings.numberedheadings.param.start-numbering-with.label=Start numbering with
nl.avisi.numbered-headings.numberedheadings.param.start-numbering-with.desc=The value to start numbering with
nl.avisi.numbered-headings.numberedheadings.param.start-numbering-with.invalid-number='Start numbering with' value is not a valid number
nl.avisi.numbered-headings.numberedheadings.param.start-numbering-with.not-supported=Selected number format does not support numbering starting at {0}
nl.avisi.numbered-headings.numberedheadings.param.start-numbering-at.label=Start numbering at
nl.avisi.numbered-headings.numberedheadings.param.start-numbering-at.desc=The heading to start numbering at
nl.avisi.numbered-headings.numberedheadings.param.skip-headings.label=Skip headings
nl.avisi.numbered-headings.numberedheadings.param.skip-headings.desc=Comma separated list of headings which should be skipped. Example: h5, h6
nl.avisi.numbered-headings.numberedheadings.param.h1.label=h1 format
nl.avisi.numbered-headings.numberedheadings.param.h1.desc=Example: Chapter [h1.decimal].
nl.avisi.numbered-headings.numberedheadings.param.h2.label=h2 format
nl.avisi.numbered-headings.numberedheadings.param.h2.desc=Example: Paragraph [h1.decimal].[h2.upper-latin].
nl.avisi.numbered-headings.numberedheadings.param.h3.label=h3 format
nl.avisi.numbered-headings.numberedheadings.param.h3.desc=Example: Sub-Paragraph [h3.lower-roman].
nl.avisi.numbered-headings.numberedheadings.param.h4.label=h4 format
nl.avisi.numbered-headings.numberedheadings.param.h4.desc=If left empty, nothing will be rendered.
nl.avisi.numbered-headings.numberedheadings.param.h5.label=h5 format
nl.avisi.numbered-headings.numberedheadings.param.h5.desc=You can use any of the formatters from the dropdown above.
nl.avisi.numbered-headings.numberedheadings.param.h6.label=h6 format

